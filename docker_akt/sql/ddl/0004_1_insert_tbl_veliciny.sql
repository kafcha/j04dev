INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(92, 2, 'PLC_main_Info.infraTempPreheat', 'teplota předehřev', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(36, 4, 'PLC_main_Info.Extruder.tempSpindle[0]', 'Topná zóna šneku 2 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(4, 2, 'PLC_main_Info.extruder.speedSpindle', 'otáčky šneku extruder', 'ot/min', NULL, NULL, NULL, NULL, 0.01000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(6, 2, 'PLC_main_Info.extruder.pressurePoint[3]', 'spád čerpadla extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(37, 4, 'PLC_main_Info.Extruder.tempSpindle[1]', 'Topná zóna šneku 3 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(29, 4, 'PLC_main_Info.Extruder.powerMotor', 'Proud motoru extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(8, 2, 'PLC_main_Info.diagnostic.extruderTemp_winding[0]', 'teplota vinutí motoru extruderu 1', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(9, 2, 'PLC_main_Info.diagnostic.extruderTemp_winding[1]', 'teplota vinutí motoru extruderu 2', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(10, 2, 'PLC_main_Info.diagnostic.extruderTemp_winding[2]', 'teplota vinutí motoru extruderu 3', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(11, 2, 'PLC_main_Info.diagnostic.extruderTemp_bearing[0]', 'teplota ložiska motoru extruderu 1', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(12, 2, 'PLC_main_Info.diagnostic.extruderTemp_bearing[1]', 'teplota ložiska motoru extruderu 2', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(13, 2, 'PLC_main_Info.diagnostic.extruderTemp_bearing_gearbox[0]', 'teplota ložiska převodovky extruderu 1', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(14, 2, 'PLC_main_Info.diagnostic.extruderTemp_bearing_gearbox[1]', 'teplota ložiska převodovky extruderu 2', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(15, 2, 'PLC_main_Info.diagnostic.extruderTemp_oil', 'teplota oleje převodovky', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(38, 2, 'PLC_main_Info.winderCurrent[0]', 'Proud motoru navíječe 1', 'A', NULL, NULL, NULL, NULL, 0.01000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(83, 2, 'PLC_main_Info.extruder.currentPump', 'proud čerpadla extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(18, 2, 'PLC_main_Info.extruder.tempPoint[0]', 'teplota taveniny před filtrem', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(40, 2, 'PLC_main_Info.winderTorque[0]', 'momemt navíječe 1', 'Nm', NULL, NULL, NULL, NULL, 0.01000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(20, 2, 'PLC_main_Info.extruder.tempPoint[4]', 'teplota taveniny za čerpadlem', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(21, 2, 'PLC_main_Info.extruderTemp[0]', 'teplota na šneku extruderu 1', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(22, 2, 'PLC_main_Info.extruderTemp[1]', 'teplota na šneku extruderu 2', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(23, 2, 'PLC_main_Info.extruderTemp[2]', 'teplota na šneku extruderu 3', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(24, 2, 'PLC_main_Info.extruderTemp[3]', 'teplota na šneku extruderu 4', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(25, 2, 'PLC_main_Info.extruderTemp[4]', 'teplota na šneku extruderu 5', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(26, 2, 'PLC_main_Info.extruderTemp[5]', 'teplota na šneku extruderu 6', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(19, 2, 'PLC_main_Info.extruder.tempPoint[2]', 'teplota taveniny za filtrem', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(27, 3, 'PLC_main_Info.extruder.speedSpindle', 'otáčky šneku extruderu', 'ot/min', NULL, NULL, NULL, NULL, 0.01000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(31, 4, 'PLC_main_Info.Extruder.tempGearBox[1]', 'Teplota ložiska převodovky extruderu', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(34, 4, 'PLC_main_Info.Extruder.pressurePoint[2]', 'Skutečný tlak za čerpadlem extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(32, 4, 'PLC_main_Info.Extruder.pressurePoint[0]', 'Skutečný tlak před filtrem extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(33, 4, 'PLC_main_Info.Extruder.pressurePoint[1]', 'Skutečný tlak před čerpadlem extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(35, 4, 'PLC_main_Info.Extruder.pressurePoint[3]', 'Skutečný tlak před směšovačem extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(42, 4, 'PLC_main_Info.Extruder.tempSpindleMonitor', 'Topná zóna šneku 1 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(41, 2, 'PLC_main_Info.winderTorque[1]', 'momemt navíječe 2', 'Nm', NULL, NULL, NULL, NULL, 0.01000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(44, 4, 'PLC_main_Info.Coextruder.powerMotor', 'Proud motoru koextruderu', 'A', NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(45, 4, 'PLC_main_Info.Coextruder.tempSpindle[0]', 'Topná zóna šneku 1 skutečná koextruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(46, 4, 'PLC_main_Info.Coextruder.tempSpindle[1]', 'Topná zóna šneku 2 skutečná koextruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(47, 4, 'PLC_main_Info.Coextruder.tempSpindle[2]', 'Topná zóna šneku 3 skutečná koextruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(48, 4, 'PLC_main_Info.Coextruder.tempSpindle[3]', 'Topná zóna šneku 4 skutečná koextruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(49, 4, 'PLC_main_Info.Coextruder.tempSpindle[4]', 'Topná zóna šneku 5 skutečná koextruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(50, 4, 'PLC_main_Info.Coextruder.tempSpindle[5]', 'Topná zóna šneku 6 skutečná koextruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(51, 4, 'PLC_main_Info.Coextruder.tempSpindle[6]', 'Topná zóna šneku 7 skutečná koextruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(52, 4, 'PLC_main_Info.Coextruder.tempSpindle[7]', 'Topná zóna šneku 8 skutečná koextruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(53, 4, 'PLC_main_Info.Coextruder.tempSpindle[8]', 'Topná zóna šneku 9 skutečná koextruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(54, 4, 'PLC_main_Info.Coextruder.tempSpindle[9]', 'Topná zóna šneku 10 skutečná koextruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(55, 4, 'PLC_main_Info.Coextruder.tempSpindle[10]', 'Topná zóna šneku 11 skutečná koextruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(56, 4, 'PLC_main_Info.Coextruder.tempSpindle[11]', 'Topná zóna šneku 12 skutečná koextruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(57, 4, 'PLC_main_Info.Coextruder.tempSpindle[12]', 'Topná zóna šneku 13 skutečná koextruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(58, 4, 'PLC_main_Info.Coextruder.tempSpindle[13]', 'Topná zóna šneku 14 skutečná koextruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(28, 4, 'PLC_main_Info.Extruder.speedSpindle', 'Otáčky šneku extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(59, 4, 'PLC_main_Info.Coextruder.pressurePoint[0]', 'Skutečný tlak před filtrem koextruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(60, 4, 'PLC_main_Info.Coextruder.pressurePoint[1]', 'Skutečný před čerpadlem koextruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(43, 4, 'PLC_main_Info.Coextruder.speedSpindle', 'Otáčky šneku koextruder', 'ot/min', NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(39, 2, 'PLC_main_Info.winderCurrent[1]', 'Proud motoru navíječe 2', 'A', NULL, NULL, NULL, NULL, 0.01000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(30, 4, 'PLC_main_Info.Extruder.tempGearBox[0]', 'Teplota oleje převodovky extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(61, 4, 'PLC_main_Info.Extruder.tempSpindle[2]', 'Topná zóna šneku 4 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(62, 4, 'PLC_main_Info.Extruder.tempSpindle[3]', 'Topná zóna šneku 5 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(63, 4, 'PLC_main_Info.Extruder.tempSpindle[4]', 'Topná zóna šneku 6 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(64, 4, 'PLC_main_Info.Extruder.tempSpindle[5]', 'Topná zóna šneku 7 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(7, 2, 'PLC_main_Info.extruder.currentMotor', 'proud motoru extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(3, 2, 'PLC_main_Info.extruder.pressurePoint[2]', 'aktuální tlak za filtrem extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(82, 2, 'ABB_Drive[2].actualValue1', 'rychlost čerpadla extruder', NULL, NULL, NULL, NULL, NULL, 0.00211068);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(65, 4, 'PLC_main_Info.Extruder.tempSpindle[6]', 'Topná zóna šneku 8 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(66, 4, 'PLC_main_Info.Extruder.tempSpindle[7]', 'Topná zóna šneku 9 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(67, 4, 'PLC_main_Info.Extruder.tempSpindle[8]', 'Topná zóna šneku 10 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(68, 4, 'PLC_main_Info.Extruder.tempSpindle[9]', 'Topná zóna šneku 11 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(69, 4, 'PLC_main_Info.Extruder.tempSpindle[10]', 'Topná zóna šneku 12 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(70, 4, 'PLC_main_Info.Extruder.tempSpindle[11]', 'Topná zóna šneku 13 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(71, 4, 'PLC_main_Info.Extruder.tempSpindle[12]', 'Topná zóna šneku 14 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(72, 4, 'PLC_main_Info.Extruder.tempSpindle[13]', 'Topná zóna šneku 15 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(73, 4, 'PLC_main_Info.Extruder.tempSpindle[14]', 'Topná zóna šneku 16 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(74, 4, 'PLC_main_Info.Extruder.tempSpindle[15]', 'Topná zóna šneku 17 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(75, 4, 'PLC_main_Info.Extruder.tempSpindle[16]', 'Topná zóna šneku 18 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(76, 4, 'PLC_main_Info.Extruder.tempSpindle[17]', 'Topná zóna šneku 19 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(77, 4, 'PLC_main_Info.Extruder.tempSpindle[18]', 'Topná zóna šneku 20 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(78, 4, 'PLC_main_Info.Extruder.tempSpindle[19]', 'Topná zóna šneku 21 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(79, 4, 'PLC_main_Info.Extruder.tempSpindle[20]', 'Topná zóna šneku 22 skutečná extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(5, 2, 'PLC_main_Info.extruder.pressurePoint[0]', 'aktuální tlak před filtrem extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(16, 2, 'PLC_main_Info.extruder.pressurePoint[4]', 'tlak za čerpadlem extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(17, 2, 'PLC_main_Info.extruder.pressurePoint[5]', 'tlak za feedblockem extruder', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(85, 4, 'PLC_main_Info.speedLine', 'Rychlost linky', 'm/min', NULL, NULL, NULL, NULL, 0.01000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(84, 4, 'PLC_main_Info.speedMaster', 'Rychlost kalandru V2', 'm/min', NULL, NULL, NULL, NULL, 0.01000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(93, 2, 'PLC_main_Info.infraTempPreheatDown1', 'teplota předehřev spodní 1', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(94, 2, 'PLC_main_Info.infraTempPreheatDown2', 'teplota předehřev spodní 2', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(95, 2, 'PLC_main_Info.infraTempSeal', 'teplota infraohřevu', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(89, 4, 'PLC_main_ParamCommon.ratioDraw[0]', 'Rychlost 1. odtah', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(90, 4, 'PLC_main_ParamCommon.ratioDraw[1]', 'Rychlost 2. odtah', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(86, 4, 'PLC_main_Info.tempWater[0]', 'Teplota horní válec V1', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(96, 2, 'PLC_main_Info.infraTempSide_L', 'teplota bočního infraohřevu vlevo', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(97, 2, 'PLC_main_Info.infraTempSide_R', 'teplota bočního infraohřevu vpravo', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(87, 4, 'PLC_main_Info.tempWater[1]', 'Teplota prostřední válec V2', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(88, 4, 'PLC_main_Info.tempWater[2]', 'Teplota spodní válec V3', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(91, 4, 'ABB_Drive[2].actualValue1', 'rychlost čerpadla extruder', NULL, NULL, NULL, NULL, NULL, 0.00252247);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(109, 2, 'PLC_main_Info.calenderDrumPressure[1]', 'tlak mezi 1 a 2 kalandrovacím válcem vpravo', NULL, NULL, NULL, NULL, NULL, 0.01000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(110, 4, 'PLC_main_Info.gapDrum1_2[0]', 'vzdálenost kal V1 a V2 levé strana', 'mm', NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(111, 4, 'PLC_main_Info.gapDrum1_2[1]', 'vzdálenost kal V1 a V2 pravá strana', 'mm', NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(118, 2, 'PLC_main_Param.extruderTemp[0]', 'Teplota zona extruder žádaná 1', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(119, 2, 'PLC_main_Param.extruderTemp[1]', 'Teplota zona extruder žádaná 2', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(112, 2, 'PLC_main_Info.calenderDrumSpeed[0]', 'aktuální rychlost kalandrovaciho valce 1', NULL, NULL, NULL, NULL, NULL, 0.01000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(113, 2, 'PLC_main_Info.calenderDrumSpeed[1]', 'aktuální rychlost kalandrovaciho valce 2', NULL, NULL, NULL, NULL, NULL, 0.01000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(98, 2, 'PLC_main_ParamCommon.infraHeaterPower[0]', 'Infra ohřev 1', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(99, 2, 'PLC_main_ParamCommon.infraHeaterPower[1]', 'Infra ohřev 2', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(100, 2, 'PLC_main_ParamCommon.infraHeaterPower[2]', 'Infra ohřev 3', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(101, 2, 'PLC_main_ParamCommon.infraHeaterPower[3]', 'Infra ohřev 4', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(102, 2, 'PLC_main_ParamCommon.infraHeaterPower[4]', 'Infra ohřev 5', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(103, 2, 'PLC_main_ParamCommon.infraHeaterPower[5]', 'Infra ohřev 6', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(104, 2, 'PLC_main_ParamCommon.infraHeaterPower[6]', 'Infra ohřev 7', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(105, 2, 'PLC_main_ParamCommon.infraHeaterPower[7]', 'Infra ohřev 8', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(106, 2, 'PLC_main_ParamCommon.infraHeaterPower[8]', 'Infra ohřev 9', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(107, 2, 'PLC_main_ParamCommon.infraHeaterPower[9]', 'Infra ohřev 10', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(108, 2, 'PLC_main_Info.calenderDrumPressure[0]', 'tlak mezi 1 a 2 kalandrovacím válcem vlevo', NULL, NULL, NULL, NULL, NULL, 0.01000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(114, 2, 'PLC_main_Info.calenderDrumSpeed[2]', 'aktuální rychlost kalandrovaciho valce 3', NULL, NULL, NULL, NULL, NULL, 0.01000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(115, 2, 'PLC_main_Info.calenderDrumTorque[0]', 'moment motoru kalandrovaciho valce 1', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(116, 2, 'PLC_main_Info.calenderDrumTorque[1]', 'moment motoru kalandrovaciho valce 2', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(117, 2, 'PLC_main_Info.calenderDrumTorque[2]', 'moment motoru kalandrovaciho valce 3', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(120, 2, 'PLC_main_Param.extruderTemp[2]', 'Teplota zona extruder žádaná 3', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(121, 2, 'PLC_main_Param.extruderTemp[3]', 'Teplota zona extruder žádaná 4', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(122, 2, 'PLC_main_Param.extruderTemp[4]', 'Teplota zona extruder žádaná 5', NULL, NULL, NULL, NULL, NULL, 0.10000000);
INSERT INTO sqlj_tech_data.tbl_veliciny
(id, id_stroje, velic_plc_nazev, uziv_nazev, jednotka, min_warning, max_warning, min_error, max_error, koeficient)
VALUES(123, 2, 'PLC_main_Param.extruderTemp[5]', 'Teplota zona extruder žádaná 6', NULL, NULL, NULL, NULL, NULL, 0.10000000);
