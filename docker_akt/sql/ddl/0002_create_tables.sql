CREATE TABLE sqlj_tech_data.tbl_stroje (
	id serial NOT NULL,
	zavod varchar(2) NULL,
	nazev text NULL,
	popis text NULL,
	agent_nazev text NULL,
	ip_adresa text NULL,
	tcp_port int4 NULL,
	destination_station int4 NULL,
	CONSTRAINT pk_stroje PRIMARY KEY (id),
	CONSTRAINT un_stroje_stroj UNIQUE (nazev)
)
WITH (
	OIDS=TRUE
);

CREATE TABLE sqlj_tech_data.tbl_veliciny (
	id serial NOT NULL,
	id_stroje int4 NULL,
	velic_plc_nazev text NULL, -- nezev veliciny pro vycitani
	uziv_nazev text NULL, -- uzivatelsky nazev
	jednotka text NULL,
	min_warning numeric(20,8) NULL,
	max_warning numeric(20,8) NULL,
	min_error numeric(20,8) NULL,
	max_error numeric(20,8) NULL,
	koeficient numeric(20,8) NOT NULL DEFAULT 1, -- prepoctovy koeficient pro ukladani dat�napr otacky jsou vycitany jako 1550 ale uklada je chceme jako 155, pak koeficient bude 0.1
	CONSTRAINT pk_veliciny PRIMARY KEY (id),
	CONSTRAINT fk_veliciny_stroj FOREIGN KEY (id_stroje) REFERENCES sqlj_tech_data.tbl_stroje(id)
)
WITH (
	OIDS=TRUE
);

-- Column comments

COMMENT ON COLUMN sqlj_tech_data.tbl_veliciny.velic_plc_nazev IS 'nezev veliciny pro vycitani';
COMMENT ON COLUMN sqlj_tech_data.tbl_veliciny.uziv_nazev IS 'uzivatelsky nazev';
COMMENT ON COLUMN sqlj_tech_data.tbl_veliciny.koeficient IS 'prepoctovy koeficient pro ukladani dat
napr otacky jsou vycitany jako 1550 ale uklada je chceme jako 155, pak koeficient bude 0.1';


CREATE TABLE sqlj_tech_data.tbl_def_vycitani (
	id serial NOT NULL,
	dt_zal timestamptz NULL DEFAULT now(),
	id_veliciny int4 NULL,
	interval_s numeric(10,2) NULL, -- interval vycitani v sekundach
	doba_uchovani_dny int4 NULL, -- jak dlouho se maji data uchovavat �pokud neni vyplneno, tak navzdy
	aktual bool NOT NULL DEFAULT true, -- urcuje jestli se maji data vycitat
	CONSTRAINT pk_def_vycitani PRIMARY KEY (id),
	CONSTRAINT fk_def_vycitani_velicina FOREIGN KEY (id_veliciny) REFERENCES sqlj_tech_data.tbl_veliciny(id)
)
WITH (
	OIDS=TRUE
);

-- Column comments

COMMENT ON COLUMN sqlj_tech_data.tbl_def_vycitani.interval_s IS 'interval vycitani v sekundach';
COMMENT ON COLUMN sqlj_tech_data.tbl_def_vycitani.doba_uchovani_dny IS 'jak dlouho se maji data uchovavat
pokud neni vyplneno, tak navzdy';
COMMENT ON COLUMN sqlj_tech_data.tbl_def_vycitani.aktual IS 'urcuje jestli se maji data vycitat';

CREATE TABLE sqlj_tech_data.tbl_data_num (
	dt timestamptz NOT NULL DEFAULT 'now'::text::timestamp(3) with time zone,
	id_definice int4 NULL,
	hodnota numeric(20,8) NULL
)
WITH (
	OIDS=TRUE
);
CREATE INDEX in_data_num_dt ON sqlj_tech_data.tbl_data_num USING btree (dt);
CREATE INDEX in_data_num_dt_def_id ON sqlj_tech_data.tbl_data_num USING btree (id_definice, dt);
COMMENT ON TABLE sqlj_tech_data.tbl_data_num IS 'numericka data';








