CREATE OR REPLACE VIEW sqlj_tech_data.v_data_numeric
AS SELECT data.oid AS vyct_oid,
    data.dt,
    data.hodnota,
    vel.uziv_nazev AS vel_uziv_nazev,
    vel.velic_plc_nazev,
    stroj.nazev AS stroj_nazev,
    def.id AS id_definice,
    stroj.id AS id_stroj,
    vel.id AS id_veliciny
   FROM sqlj_tech_data.tbl_data_num data
     LEFT JOIN sqlj_tech_data.tbl_def_vycitani def ON def.id = data.id_definice
     LEFT JOIN sqlj_tech_data.tbl_veliciny vel ON vel.id = def.id_veliciny
     LEFT JOIN sqlj_tech_data.tbl_stroje stroj ON stroj.id = vel.id_stroje;


CREATE OR REPLACE VIEW sqlj_tech_data.v_def_veliciny
AS SELECT vel.id AS id_vel,
    stroj.id AS id_stroj,
    stroj.zavod,
    stroj.nazev AS stroj_nazev,
    stroj.popis AS stroj_popis,
    stroj.agent_nazev,
    vel.velic_plc_nazev,
    vel.uziv_nazev AS velic_uziv_nazev,
    vel.koeficient
   FROM sqlj_tech_data.tbl_veliciny vel
     LEFT JOIN sqlj_tech_data.tbl_stroje stroj ON stroj.id = vel.id_stroje;

CREATE OR REPLACE VIEW sqlj_tech_data.v_def_vycitani
AS SELECT def.id AS id_def,
    vel.id AS id_vel,
    stroj.id AS id_stroj,
    stroj.zavod,
    stroj.nazev AS stroj_nazev,
    stroj.popis AS stroj_popis,
    stroj.agent_nazev,
    vel.velic_plc_nazev,
    vel.uziv_nazev AS velic_uziv_nazev,
    vel.koeficient,
    def.dt_zal,
    def.interval_s,
    def.aktual,
    def.doba_uchovani_dny
   FROM sqlj_tech_data.tbl_def_vycitani def
     LEFT JOIN sqlj_tech_data.tbl_veliciny vel ON vel.id = def.id_veliciny
     LEFT JOIN sqlj_tech_data.tbl_stroje stroj ON stroj.id = vel.id_stroje;